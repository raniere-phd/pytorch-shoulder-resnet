"""
Main

Manager the agent to train, validate, and test the model.
At the end of the operation,
a report is submited to Comet.
"""
import argparse
import logging

import comet_ml
import torch

from agent import Agent

hyper_params = {
    "device": "cuda" if torch.cuda.is_available() else "cpu",
    "num_epochs": None,
    "batch_size": None,
    "model": None,
}


def main():
    """Main loop"""
    parser = argparse.ArgumentParser(description="PyTorch Shoulder ResNet.")
    parser.add_argument(
        "--model",
        choices=["resnet18", "resnet34", "resnet50", "resnet101", "resnet152"],
        default="resnet18",
        help="Model to use",
    )
    parser.add_argument("--epochs", type=int, default=16, help="Number of epochs")
    parser.add_argument("--batch", type=int, default=32, help="Batch size")
    parser.add_argument(
        "--debug", action="store_true", help="Set logging level to DEBUG."
    )
    parser.add_argument(
        "--verbose", action="store_true", help="Set logging level to INFO."
    )
    parser.add_argument(
        "--local",
        action="store_true",
        help="Disable all network communication with the Comet.ml backend.",
    )
    args = parser.parse_args()

    logging.basicConfig()
    logger = logging.getLogger("pytorch")
    if args.verbose:
        logger.setLevel(logging.INFO)
    if args.debug:
        logger.setLevel(logging.DEBUG)

    hyper_params["model"] = args.model
    hyper_params["num_epochs"] = args.epochs
    hyper_params["batch_size"] = args.batch

    logger.info("Starting experiment ...")
    experiment = comet_ml.Experiment(
        project_name="pytorch-shoulder-resnet",
        workspace="raniere-silva",
        log_code=False,
        log_graph=False,
        auto_param_logging=False,
        auto_metric_logging=False,
        auto_output_logging=False,
        auto_log_co2=False,
        disabled=args.local,
    )
    experiment.log_parameters(hyper_params)

    logger.debug("Creating agent ...")
    agent = Agent(hyper_params, comet_logger=experiment)
    logger.debug("Creating agent completed.")

    with experiment.train():
        logger.info("Training model ...")
        agent.train()

    with experiment.validate():
        logger.info("Validating model ...")

    with experiment.test():
        logger.info("Testing model ...")
        agent.test()

    logger.info("Finishing experiment ...")
    agent.finalize()
    experiment.end()
    logger.info("Finishing experiment completed.")


if __name__ == "__main__":
    main()
