# PyTorch Template

This repository is a Machine Learning model implemented in PyTorch
to detect fracture in the Shoulder data.

## Setting Environment with Conda

Run

```
$ conda env create --file environment.yml
```

## Data

1. Visit https://www.kaggle.com/dryari5/shoulder-xray-classification.
1. Download the data and unzip it.
1. Run `ln -s /path/to/shoulder/data/ data && ls`.
   You should have `test`, `train`, `val` in the output.

## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in [Comet](https://www.comet.ml/).

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `data`

  Store the data. It should be replace with a symbolic link to the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `weights`

  Store the weights of trained model.