"""
Dataset

Includes all the functions that either preprocess or postprocess data.
"""
from torch.utils.data import DataLoader
from torchvision.datasets import ImageFolder

from torchvision import transforms

transform = transforms.Compose([
    transforms.Resize(320),
    transforms.CenterCrop(320),
    transforms.ToTensor()
])

def get_loaders(batch_size):
    """Get all loaders"""
    train = ImageFolder(
        'data/train',
        transform=transform
    )
    val = ImageFolder(
        'data/val',
        transform=transform
    )
    test = ImageFolder(
        'data/test',
        transform=transform
    )

    return [
        DataLoader(train, batch_size, shuffle=True),
        DataLoader(val, batch_size, shuffle=True),
        DataLoader(test, batch_size, shuffle=True),
    ]
